<?php

/**
 * @file
 * Contains a settings form callback for administration of CE Server.
 *
 * @ingroup ce_server
 */

/**
 * Settings form.
 */

function ce_server_settings() {
	// These two lines were so totally stolen from system_performance_settings()
	// in system.admin.inc.
	$period = drupal_map_assoc(array(0, 60, 180, 300, 600, 900, 1800, 2700, 3600, 10800, 21600, 32400, 43200, 86400, 172800, 259200), 'format_interval');
  $period[0] = '<'. t('none') .'>';
  
	return system_settings_form(
		array(
			'ce_server_status_expire' => array(
				'#type' => 'select',
				'#title' => t('Server status cache duration'),
				'#default_value' => intval(variable_get('ce_server_status_expire', 3600)),
				'#options' => $period,
				'#description' => t('The server status information may be intensive to calculate and slow down your server, but the results are unlikely to change on a moment-to-moment basis. Due to this, it&rsquo;s recommended that you allow the Compound Eye server to cache this information so that it does not have to be recalculated every time a client pings the server for information.'),
			),
			'ce_server_log_last' => array(
				'#type' => 'select',
				'#title' => t('Logged errors duration'),
				'#default_value' => intval(variable_get('ce_server_log_last', 43200)),
				'#options' => $period,
				'#description' => t('The server will return logged errors from the period selected above relative to when it was pinged. Setting this too low may result in logged errors going unnoticed; setting it too high may result in information overload.'),
			),
		)
	);
}