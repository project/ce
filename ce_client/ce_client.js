
/**
 * @file
 * The JavaScript that CE Client needs to be useful.
 *
 * @ingroup ce_client
 */

Drupal.ceClient = Drupal.ceClient || {};
Drupal.ceClient.parsers = Drupal.ceClient.parsers || {};	

Drupal.ceClient.severities = {
	0: 'ok',
	1: 'warning',
	2: 'error',
	3: 'error'
};

Drupal.ceClient.status = function(statusMsg, alreadyTranslated, args) {
	var alreadyTranslated = alreadyTranslated || false,
		args = args || {};
	$('#ce-client-status').html(alreadyTranslated ? statusMsg : Drupal.t(statusMsg, args));
};

Drupal.ceClient.fetch = function() {
	Drupal.ceClient.status('Fetching server group information&hellip;');
	// Fetch server data
	$.ajax({
		'dataType': 'json',
		// Note that we're using ?q-style paths in the URL because we're not sure
		// that the server will have clean URLs enabled.
		'url': Drupal.settings.basePath + 'index.php?q=admin/reports/ce_client/group/' + Drupal.settings.ceClient.sgid + '/json',
		'error': function(theRequest, textStatus, errorThrown) {
			Drupal.ceClient.status('Server group information fetch failed.');
			// Check theRequest.status for the HTTP status
		},
		'success': function(data, textStatus) {
			Drupal.ceClient.status(Drupal.formatPlural(data.servers.length, 'Server group information fetch successful. One server in group.', 'Server group information fetch successful. @count servers in group.'), true);
			// Work with the data;
			// Does this only work with Zen-based themes?
			$('#content-header H1.title').text(data.name);
			Drupal.settings.ceClient.servers = [];
			jQuery.each(data.servers, function(i) {
				Drupal.ceClient.status('Fetching info for %server&hellip;', false, {'%server': this.url});
				$('body').append($('<script>').attr({
					'type': 'text/javascript',
					'id': 'ce-client-script-' + i,
					'src': this.url + 'index.php?q=admin/reports/ce_server/' + i
				}));
//				$.ajax({
//					'url': this.url + 'index.php?q=admin/reports/ce_server',
//					'dataType': 'json',
//					'error': function(theRequest, textStatus, errorThrown) {
//						Drupal.ceClient.status('Server info fetch failed for %server.', false, {'%server': this.url});
//						
//						
//					},
//					'success': function(data, textStatus) {
//						Drupal.ceClient.status('Server info fetch successful for %server.', false, {'%server': this.url});
//						// Get or create the div for this site…
//						var id = 'ce-server-' + data.info.installTime,
//							theDiv = $('#' + id);
//						if (theDiv.length) {
//							theDiv.empty().removeClass();
//						}
//						else {
//							theDiv = $('<div>').attr('id', id);
//							$('#ce-client').append(theDiv);
//						}
//						// Check for protocol compatibility…
//						if (data.info.ceProtocol !== 1) {
//							theDiv.addClass('ce-error').append($('<p>').html(Drupal.t('The server data&rsquo;s protocol version is unfamiliar. The most likely reason for this is that the versions of Compound Eye Client and Compound Eye Server are not in synch. Upgrade both the client and server&rsquo;s modules to the most recently available release.')));
//						}
//						else {
//							theDiv.append($('<h2>').addClass('ce-server-name').html(Drupal.checkPlain(data.info.siteName)));
//							$.each(Drupal.ceClient.parsers, function() {
//								this(data, theDiv);
//							});
//						}
//					}
//				});
			});
		}
	});
};

Drupal.ceClient.pingReturn = function(data) {
	$('#ce-client-script-' + data.info.index).remove();
	Drupal.ceClient.status('Server info fetch successful for %server.', false, {'%server': data.info.siteName});
	// Get or create the div for this site…
	var id = 'ce-server-' + data.info.installTime,
		theDiv = $('#' + id);
	if (theDiv.length) {
		theDiv.empty().removeClass();
	}
	else {
		theDiv = $('<div>').attr('id', id);
		$('#ce-client').append(theDiv);
	}
	// Check for protocol compatibility…
	theDiv.append($('<h2>').addClass('ce-server-name').html(Drupal.checkPlain(data.info.siteName)));
	if (data.info.ceProtocol !== 2) {
		theDiv.addClass('error').append($('<p>').html(Drupal.t('The server data&rsquo;s protocol version is unfamiliar. The most likely reason for this is that the versions of Compound Eye Client and Compound Eye Server are not in synch. Upgrade both the client and server&rsquo;s modules to the most recently available release.')));
	}
	else {
		// Check for status code…
		if (data.info.status === 403) {
			theDiv.append($('<ul>').append(Drupal.ceClient.makeLi(Drupal.t('Access denied. Assure that this web browser is currently logged in to the server site via a user account with sufficient permissions to access the Compound Eye Server data.'), 2)));
		}
		else if (data.info.status !== 200) {
			theDiv.append($('<ul>').append(Drupal.t('The data returned from the server had an access code of !code, which cannot be handled. (Are you running the most recent version of the Compound Eye Client?)', {'!code': data.info.status}), 2));
		}
		else {
			$.each(Drupal.ceClient.parsers, function() {
				this(data, theDiv);
			});
		}
	}
};

Drupal.ceClient.makeLi = function(html, severity, childList) {
	childList = childList || false;
//	var theLi = $('<li>').append($('<div>').addClass(Drupal.ceClient.severities[severity]).html(html));
	console.log('severity: ' + severity);
	var theLi = $('<li>').addClass(Drupal.ceClient.severities[severity]).html(html);
	if (childList) {
		theLi.append(childList);
	}
	return theLi;
}

Drupal.ceClient.parsers.core = function(data, theDiv) {
	// Add core div
	var theList = $('<ul>');
	theDiv.append($('<div>').addClass('ce-client-core').append(theList));
	
	// Check if site is offline
	if (data.core.offline) {
		theList.append(Drupal.ceClient.makeLi(Drupal.t('Site is offline.'), 2));
	}
	
	// Check for crucial status messages
	if (data.core.status.messages.length) {
		var statusMsgs = $('<ul>'),
			largestSev = 0;
		$.each(data.core.status.messages, function() {
			statusMsgs.append(Drupal.ceClient.makeLi(Drupal.t('@title: !value', {'@title': this.title, '!value': this.value}), this.severity));
			if (this.severity > largestSev) {
				largestSev = this.severity;
			}
		});
		theList.append(Drupal.ceClient.makeLi(Drupal.t('Status messages'), largestSev, statusMsgs));
	}
	
	// Check for watchdog messages
	if (data.core.watchdog.messages.length) {
		var watchdogs = $('<ul>'),
			largestSev = 0;
		$.each(data.core.watchdog.messages, function() {
//			this.severity = parseInt(this.severity);
			watchdogs.append(Drupal.ceClient.makeLi(Drupal.t('@type : @user : !message', {'@type': this.type, '@user' : this.name, '!message': this.message}), this.severity));
			if (this.severity > largestSev) {
				largestSev = this.severity;
			}
		});
		theList.append(Drupal.ceClient.makeLi(Drupal.t('Recent log entries'), largestSev, watchdogs));		
	}
};


Drupal.behaviors.ceClient = function() {
	if (!$('#ce-client').hasClass('ceClient-processed')) {
		$('#ce-client').addClass('ceClient-processed');
		// Create status area
		$('#ce-client').empty().append($('<div>').attr('id', 'ce-client-status').text(Drupal.t('Idle')));
		// Do first fetch
		Drupal.ceClient.fetch();
	}
};
