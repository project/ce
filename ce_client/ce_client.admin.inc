<?php

/**
 * @file
 * Contains a settings form callback for administration of CE Client.
 *
 * @ingroup ce_client
 */

/**
 * Settings form. drupal_get_form() callback.
 */

function ce_client_settings() {
	// @TODO
	return array(
		'nada' => array(
			'#type' => 'markup',
			'#value' => '<em>(nothing here yet)</em>',
		),
	);
}

/**
 * Server groups list and "create new" button. drupal_get_form() callback.
 */

function ce_client_groups() {
	$rez = db_query('SELECT * FROM {ce_client_groups} ORDER BY name ASC');
	$rows = array();
	while ($group = db_fetch_object($rez)) {
		$rows[] = array(
			l($group->name, 'admin/reports/ce_client/group/' . $group->sgid),
			l(t('edit'), 'admin/settings/ce_client/group/' . $group->sgid),
			l(t('delete'), 'admin/settings/ce_client/group/delete/' . $group->sgid),
		);
	}
	
	if (count($rows) === 0) {
		$rows = array(
			array(
				array(
					'data' => t('<em>Thare are no server groups. Use the &ldquo;Create new server group&rdquo; button below to create one.</em>'),
					'colspan' => 4,
				),
			),
		);
	}	
	
	// theme table
	
	$table = theme('table', array(
		array(
			'data' => t('Group name'),
		),
		array(
			'data' => t('Actions'),
			'colspan' => 3,
		),
	), $rows/*, array('id' => 'ce-client-groups-table')*/);
	
	return array(
		'ce_client_groups_table' => array(
			'#type' => 'markup',
			'#value' => $table,
			'#weight' => 0,
		),
		// new group button
		'create_new_sg' => array(
			'#type' => 'submit',
			'#value' => t('Create new server group'),
			'#weight' => 10,
		),
	);
}

/**
 * Submission function for above.
 */

function ce_client_groups_submit($form, &$form_state) {
	if ($form_state['values']['op'] === t('Create new server group')) {
//		return drupal_get_form('ce_client_edit_group_form');
		drupal_goto('admin/settings/ce_client/group/0');
	}
}

function ce_client_delete_group_form(&$form_state, $sgid) {
	$title = db_result(db_query('SELECT name FROM {ce_client_groups} WHERE sgid = %d', $sgid));
	if ($title) {
		$form = array(
			'sgid' => array(
				'#type' => 'value',
				'#value' => $sgid,
			),
			'title' => array(
				'#type' => 'value',
				'#value' => $title,
			),
		);
		return confirm_form($form, t('Are you sure you want to delete the Compound Eye server group %title?', array('%title' => $title)), 'admin/settings/ce_client/groups');
	}
	else {
		drupal_set_message(t('You seem to be trying to delete a Compound Eye server group that doesn&rsquo;t exist.'));
		drupal_goto('admin/settings/ce_client/groups');
	}
}

/**
 * Submission function for above.
 */

function ce_client_delete_group_form_submit($form, &$form_state) {
	db_query('DELETE FROM {ce_client_groups} WHERE sgid = %d', $form_state['values']['sgid']);
	drupal_set_message(t('The Compound Eye server group %title has been deleted.', array('%title' => $form_state['values']['title'])));
	drupal_goto('admin/settings/ce_client/groups');
}

/**
 * Group editing form
 */

function ce_client_edit_group_form($form_state, $sgid = NULL) {
	if ($sgid !== NULL) {
		$sgid = intval($sgid);
		if ($sgid === 0) {
			$sgid = NULL;
		}
	}
	$form = array(
		'title' => array(
			'#type' => 'textfield',
			'#title' => t('Group name'),
			'#maxlength' => 255,
			'#required' => TRUE,
			'#weight' => 0,
		),
		'servers' => array(
			'#type' => 'textarea',
			'#title' => t('Servers'),
			'#description' => t('Enter the full URL to the front page of each Drupal site. Enter one URL per line. Use the hash character (#) for comments; all characters on a line after a hash character will be ignored.'),
			'#rows' => 10,
			'#weight' => 10,
			'#default_value' => '',
			'#resizable' => TRUE,
		),
		'submit' => array(
			'#type' => 'submit',
			'#value' => t('Save'),
			'#weight' => 20,
			'#attributes' => array(
				'class' => 'form-submit',
			),
		),
////		'delete' => array(
////			'#type' => 'submit',
////			'#value' => t('Delete'),
////			'#weight' => 30,
////			'#attributes' => array(
////				'class' => 'form-submit',
////			),
////		),
		'sgid' => array(
			'#type' => 'value',
			'#value' => $sgid,
		),
	);
	
	if ($sgid !== NULL) {
		$rez = db_query('SELECT name, servers FROM {ce_client_groups} WHERE sgid = %d', $sgid);
		if ($group = db_fetch_array($rez)) {
			$form['title']['#default_value'] = $group['name'];
			$form['servers']['#default_value'] = $group['servers'];
			$form['delete'] = array(
				'#type' => 'submit',
				'#value' => t('Delete'),
				'#weight' => 30,
				'#attributes' => array(
					'class' => 'form-submit',
				),
			);
		}
		else {
			$form['sgid']['#value'] = NULL;
		}
	}

	return $form;
}

/**
 * Validation function for above.
 */

/*function ce_client_edit_group_form_validate($form, &$form_state) {
	if ($form_state['op']['value'] === t('Save')) {
		// Check that the URLs in the servers field are valid
		$form_state['values']['good_urls'] = array();
		// First, Splitty McSpliterstein:
		$to_check = explode("/n", &$form_state['values']['servers']);
		array_map('trim', $to_check);
		foreach ($to_check as $checking) {
			if ($
			if (!valid_url($checking)) {
				form_set_error('servers', t('%url does not appear to be a valid URL.', array('%url' => $checking)));
			}
			else {
				$form_state['values']['good_urls'][] = $checking;
			}
		}
	}
}*/

/**
 * Submission function for above.
 */

function ce_client_edit_group_form_submit($form, &$form_state) {
	if ($form_state['values']['op'] === t('Save')) {
		$record = array(
			'name' => $form_state['values']['title'],
			'servers' => $form_state['values']['servers'],
		);
		if ($form_state['values']['sgid'] === NULL) {
			drupal_write_record('ce_client_groups', $record);
		}
		else {
			$record['sgid'] = $form_state['values']['sgid'];
			drupal_write_record('ce_client_groups', $record, 'sgid');
			
			// Flush old server URLs
//			db_query('DELETE FROM {ce_client_servers} WHERE sgid = %d', $record['sgid']);
		}
		
/*		$weight = 0;
	
		// Now insert URLs
		foreach ($form_state['values']['good_urls'] as $url) {
			$server_record = array(
				'sgid' => $record['sgid'],
				'url' => $url,
				'weight' => $weight++,
			);
			drupal_write_record('ce_client_servers', $server_record);
		}
		*/
		drupal_set_message(t('The configuration options have been saved.'));
		drupal_goto('admin/settings/ce_client/groups');
	}
	elseif ($form_state['values']['op'] === t('Delete')) {
		drupal_goto('admin/settings/ce_client/group/delete/' . $form_state['values']['sgid']);
//		return drupal_get_form('ce_client_delete_group_form', $form_state['values']['sgid']);
	}
}